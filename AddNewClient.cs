#region ManageClient
        [HttpPost("addNewClient")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "recruiter")]
        public async Task<IActionResult> AddNewClient([FromBody]UpdateClientViewModal command)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { Success = false, Message = "Parameter can not be null" });
                }

                string EmployerId = "";
                if (command.ClientId == null)
                {
                    //add client
                    command.Email = command.Email.ToLower();

                    //check if username is unique
                    bool isUniqueEmail = await _authenticationService.UniqueEmail(command.Email);
                    if (!isUniqueEmail)
                    {
                        return Json(new { Success = false, Message = "This email is currently in use" });
                    }

                    EmployerId = await _authenticationService.AddEmployer(command);

                    var newClient = (await _authenticationService.AddEmployerAsClientAsync(EmployerId, _userAppContext.CurrentUserId));

                    return Json(new { Success = true, Message = "Successful", newClient });
                }
                return Json(new { Success = false, Message = "OOPS! Something Went Wrong" });
            }
            catch (ApplicationException e)
            {
                return Json(new { IsSuccess = false, e.Message });
            }
        }


[HttpPost("verifyClientToken")]
        public async Task<ActionResult> VeifyClientToken([FromQuery]string recruiterEmail, [FromQuery]string clientEmail, [FromQuery]string resetPasswordToken)
        {
            try
            {
                bool isTokenValid = false;
                var recruiter = new Recruiter();
                var client = new Employer();
                bool isClientAuthorised = false;
                if (recruiterEmail != null)
                {
                    recruiter = await _authenticationService.GetRecruiterFromEmail(recruiterEmail);
                }
                if (clientEmail != null)
                {
                    client = await _authenticationService.GetEmployerFromEmail(clientEmail);
                }

                if (recruiter != null && client != null)
                {
                    isClientAuthorised = await _authenticationService.IsRecruiterAuthorised(client.Id, recruiter.Id);
                    if (!isClientAuthorised)
                    {
                        throw new ArgumentException("Token is invalid or has expired please contact your recruiter");
                    }
                    isTokenValid = await _authenticationService.VerifyToken(clientEmail, resetPasswordToken);
                }
                return Json(new { Success = true, isTokenValid });
            }
            catch
            {
                return Json(new { Success = false });
            }
        }

        [HttpPost("validateInvitation")]
        public async Task<IActionResult> ValidateInvitation([FromQuery]string recruiterEmail, [FromQuery]string clientEmail, [FromQuery]string resetPasswordToken, [FromBody] string newPassword)
        {
            try
            {
                if (clientEmail == null) throw new ArgumentException("Invalid Email");
                string token = resetPasswordToken != null ? resetPasswordToken : throw new ArgumentException("Invalid Token");
                string newUserPassword = newPassword != null ? newPassword : throw new ArgumentException("Invalid Password");
                bool isTokenValid = await _authenticationService.VerifyToken(clientEmail, resetPasswordToken);
                if (isTokenValid)
                {

                    bool isVerified = await _authenticationService.VerifyEmail("employer", "", clientEmail);

                    var recruiter = await _authenticationService.GetRecruiterFromEmail(recruiterEmail);

                    var client = await _authenticationService.GetEmployerFromEmail(clientEmail);

                    if (recruiter == null || client == null || !isVerified)
                    {
                        throw new ArgumentException("Token is invalid or has expired");
                    }
                    if (!await _authenticationService.IsRecruiterAuthorised(client.Id, recruiter.Id))
                    {
                        throw new ArgumentException("Token is invalid or has expired"); //client is not allowed 
                    }

                    await _authenticationService.ResetPassword(clientEmail, newUserPassword);

                    var authenticateUser = await _authenticationService.LoginAsync(clientEmail, newUserPassword);

                    await _authenticationService.UpdateClientStatus(recruiter.Id, client.Id, InvitationStatus.Active);

                    return Json(new { IsSuccess = true, isEmailVerified = true, Token = authenticateUser });
                }
                else
                {
                    throw new ArgumentException("Token is invalid or has expired");
                }
            }
            catch (Exception e)
            {
                if (e is ApplicationException || e is ArgumentException)
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        Message = e.Message
                    });
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        Message = "Error"
                    });
                }
            }
        }
        #endregion