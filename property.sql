SELECT p.Id AS Id, p.Name AS PropertyName, phv.Value AS HomeValue INTO #tempProperty FROM OwnerProperty op 
INNER JOIN Property p ON op.PropertyId = p.Id
INNER JOIN PropertyHomeValue phv ON p.Id = phv.PropertyId
WHERE op.OwnerId = 1426 AND phv.IsActive = 1 
------------------------------------------------------------
DROP TABLE #tempProperty 
DECLARE @propertyId char(11)
SET ROWCOUNT 0

SELECT p.Id AS Id, p.Name AS PropertyName, phv.Value AS HomeValue INTO #tempProperty FROM OwnerProperty op 
INNER JOIN Property p ON op.PropertyId = p.Id
INNER JOIN PropertyHomeValue phv ON p.Id = phv.PropertyId
WHERE op.OwnerId = 1426 AND phv.IsActive = 1 

SET ROWCOUNT 1
SELECT @propertyId = Id FROM #tempProperty

WHILE @@rowcount <> 0
BEGIN
    SET ROWCOUNT 0
    SELECT CASE 
				WHEN trt.Name = 'Weekly' 
					THEN (prp.PaymentAmount * DATEDIFF(week, prp.StartDate, prp.EndDate))
				WHEN trt.Name = 'Fortnightly' 
					then (prp.PaymentAmount * DATEDIFF(week, prp.StartDate, prp.EndDate) / 2)
				WHEN trt.Name = 'Monthly'
					THEN (prp.PaymentAmount * DATEDIFF(month, prp.StartDate, prp.EndDate))
				ELSE (prp.PaymentAmount * 52) END AS TotalRentalPayment
				FROM TenantProperty prp inner join TargetRentType trt ON prp.PaymentFrequencyId = trt.Id 
				WHERE prp.PropertyId = @propertyId
    
	DELETE #tempProperty WHERE Id = @propertyId

    SET ROWCOUNT 1
    SELECT @propertyId = Id FROM #tempProperty
END
SET ROWCOUNT 0

------------------------------------------------------------

SELECT p.Id AS Id, p.Name AS PropertyName, phv.Value AS HomeValue INTO #tempProperty FROM OwnerProperty op 
INNER JOIN Property p ON op.PropertyId = p.Id
INNER JOIN PropertyHomeValue phv ON p.Id = phv.PropertyId
INNER JOIN PropertyFinance pf on p.Id = pf.PropertyId
WHERE op.OwnerId = 1426 AND phv.IsActive = 1 

SET ROWCOUNT 1
SELECT @propertyId = Id FROM #tempProperty

WHILE @@rowcount <> 0
BEGIN
    SET ROWCOUNT 0
    SELECT  
				pf.TotalExpense as Yield
				FROM Property p inner join PropertyFinance pf ON p.Id = pf.PropertyId
				WHERE p.Id = @propertyId
    
	DELETE #tempProperty WHERE Id = @propertyId

    SET ROWCOUNT 1
    SELECT @propertyId = Id FROM #tempProperty
END
SET ROWCOUNT 0
------------------------------------------------------------

DROP TABLE #tempProperty 
DECLARE @propertyId char(11)
SET ROWCOUNT 0

SELECT p.Id AS Id, p.Name AS PropertyName, phv.Value AS HomeValue INTO #tempProperty FROM OwnerProperty op 
INNER JOIN Property p ON op.PropertyId = p.Id
INNER JOIN PropertyHomeValue phv ON p.Id = phv.PropertyId
WHERE op.OwnerId = 1426 AND phv.IsActive = 1 

SET ROWCOUNT 1
SELECT @propertyId = Id FROM #tempProperty

WHILE @@rowcount <> 0
BEGIN
    SET ROWCOUNT 0
    SELECT p.Name as PropertyName, per.FirstName,per.LastName, prp.PaymentAmount as PaymentAmount ,trt.Name as PaymentType , CASE 
				WHEN trt.Name = 'Weekly' 
					THEN (prp.PaymentAmount * DATEDIFF(week, prp.StartDate, prp.EndDate))
				WHEN trt.Name = 'Fortnightly' 
					then (prp.PaymentAmount * DATEDIFF(week, prp.StartDate, prp.EndDate) / 2)
				WHEN trt.Name = 'Monthly'
					THEN (prp.PaymentAmount * DATEDIFF(month, prp.StartDate, prp.EndDate))
				ELSE (prp.PaymentAmount * 52) END AS TotalRentalPayment
				FROM Property p INNER JOIN TenantProperty prp ON p.Id = prp.PropertyId 
				INNER JOIN TargetRentType trt ON prp.PaymentFrequencyId = trt.Id 
				INNER JOIN Person per on per.Id = prp.TenantId
				WHERE prp.PropertyId = @propertyId
    
	DELETE #tempProperty WHERE Id = @propertyId

    SET ROWCOUNT 1
    SELECT @propertyId = Id FROM #tempProperty
END
SET ROWCOUNT 0
