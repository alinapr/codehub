     [HttpPost("updateEmployerPhoto")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "employer, recruiter")]
        public async Task<ActionResult> UpdateEmployerPhoto()
        {
            IFormFile file = Request.Form.Files[0];
            try
            {
                if (await _profileService.UpdateEmployerPhoto(_userAppContext.CurrentUserId, file))
                {
                    var user = await _profileService.GetEmployerProfile(_userAppContext.CurrentUserId, _userAppContext.CurrentRole);

                    string newFileName = user.ProfilePhoto;
                    string newFileUrl = user.ProfilePhotoUrl;

                    return Json(new { Success = true, newFileName, newFileUrl });
                }
                return Json(new { Success = false, Message = "Error while uploading Profile Photo" });
            }
            catch (Exception e)
            {
                return Json(new { Success = false, Message = "Error while uploading Profile Photo" });
            }
        }

        [HttpPost("updateEmployerVideo")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "employer, recruiter")]
        public async Task<IActionResult> UpdateEmployerVideo()
        {
            IFormFile file = Request.Form.Files[0];
            string userId = _userAppContext.CurrentUserId;

            if (await _profileService.AddEmployerVideo(userId, file))
            {
                string videoName = (await _profileService.GetEmployerProfile(userId, _userAppContext.CurrentRole)).VideoName;
                return Json(new { Success = true, videoName });
            }

            return Json(new { Success = false, Message = "File save failed" });
        }

        [HttpGet("getEmployerProfileImage")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "employer, recruiter")]
        public async Task<ActionResult> GetWorkSample(string Id)
        {
            var employerProfile = (await _userDocumentRepository.Get(x => x.UserId == _userAppContext.CurrentUserId && x.Status == 1 && x.DocumentType == 1 && x.IsDeleted == false)).ToList();

            string employerProfileFilePath = Path.Combine(_environment.ContentRootPath, _profileImageFolder);

            return Json(new { Success = true, employerProfile, employerProfileFilePath });
        }

        [HttpGet("getEmployerProfileImages")]
        public ActionResult GetWorkSampleImage(string Id)
        {
            var dir = Path.Combine(_environment.ContentRootPath, _profileImageFolder + Id);
            string ext = System.IO.Path.GetExtension(dir).ToLower();
            Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);

            return base.PhysicalFile(dir, regKey.GetValue("Content Type").ToString());
        }
        